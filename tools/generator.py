import json
import argparse
from faker import Factory
import random
import csv

SHAWARMAS = []
USERS = []


def generate_shawerma(count):

    fake = Factory.create('en_US')

    for i in xrange(count):
        SHAWARMAS.append([
            fake.company(),                                         #name
            random.randint(1, 5),                                   #rating
            fake.street_address(),                                  #address
            random.randint(0, 1),                                   #health_inspection
            '%.4f' % (55.55 + random.randint(0, 3*10**4)/10.0**5),  #latitude
            '%.4f' % (37.45 + random.randint(0, 3*10**4)/10.0**5)   #longitude
        ])

    with open('shawarmas.csv', 'wb') as w:
        writer = csv.writer(w, lineterminator='\n')
        for shawarma in SHAWARMAS:
            writer.writerow(shawarma)


def generate_reviews(count):

    assert count < len(SHAWARMAS)*len(USERS), 'integrity error'

    fake = Factory.create('en_US')
    reviews = []

    matrix = []

    for _ in xrange(len(USERS)):
        matrix.append([0 for i in xrange(len(SHAWARMAS))])

    for i in xrange(count):
        while(1):
            user_id = random.randint(1, len(USERS))
            shawarma_id = random.randint(1, len(SHAWARMAS))

            if matrix[user_id - 1][shawarma_id - 1] == 0:
                matrix[user_id - 1][shawarma_id - 1] = 1
                break
            else:
                continue

        reviews.append([
            random.randint(1, 5),                           #rating
            fake.text(max_nb_chars=600),                    #text
            fake.iso8601(),                                 #pub_date
            user_id,                                        #author_id
            shawarma_id                                     #shawarma_id
        ])

    with open('reviews.csv', 'wb') as w:
        writer = csv.writer(w, lineterminator='\n')
        for review in reviews:
            writer.writerow(review)

def generate_users(count):

    fake = Factory.create('en_US')

    for i in xrange(count):
        USERS.append([
            'youshallnotpass',                          #password
            fake.iso8601(),                             #last login
            0,                                          #is superuser
            fake.simple_profile()['username'],      
            fake.first_name(),  
            fake.last_name(),   
            fake.email(),   
            0,                                          #is staff
            0,                                          #is active
            fake.iso8601()                              #date
        ])

    with open('users.csv', 'wb') as w:
        writer = csv.writer(w, lineterminator='\n')
        for user in USERS:
            writer.writerow(user)   


if __name__ == '__main__':
    shararmas = 50
    reviews = 150
    users = 5
    generate_shawerma(shararmas)
    generate_users(users)
    generate_reviews(reviews)
