import json
import requests
import csv
from datetime import datetime
import calendar

increment = 0
SHAWARMAS = []
REVIEWS = []


def epoch_to_iso8601(timestamp):
    """
    epoch_to_iso8601 - convert the unix epoch time into a iso8601 formatted date
    >>> epoch_to_iso8601(1341866722)
    '2012-07-09T22:45:22'
    """
    return datetime.fromtimestamp(timestamp).isoformat()

def iso8601_to_epoch(datestring):
    """
    iso8601_to_epoch - convert the iso8601 date into the unix epoch time
    >>> iso8601_to_epoch("2012-07-09T22:27:50.272517")
    1341872870
    """
    return calendar.timegm(datetime.strptime(datestring, "%Y-%m-%dT%H:%M:%S.%f").timetuple())


def get_response(offset, count):
    return requests.get('https://api.vk.com/method/wall.get?owner_id=-91728729&offset=%s&count=%s' % (offset, count)).text


def get_review(i):
    response = get_response(i*100, (i+1)*100)
    response = requests.get('https://api.vk.com/method/wall.get?owner_id=-91728729&offset=%s&count=%s' % (i*100, (i+1)*100)).text

    global increment

    for k in range(1, 101):
        if (type(json.loads(response)['response'][k]) == int):
            continue

        if ('geo' in json.loads(response)['response'][k]):
            review = json.loads(response)['response'][k]['text']
            coords = json.loads(response)['response'][k]['geo']['coordinates']
            place = json.loads(response)['response'][k]['geo']['place']['title']
            date = json.loads(response)['response'][k]['date']

            increment += 1

            SHAWARMAS.append([
                place.encode('utf-8'),  # name
                4,                      # rating
                place.encode('utf-8'),  # address
                1,                      # health_inspection
                coords.split()[0],      # latitude
                coords.split()[1]       # longitude
            ])
            REVIEWS.append([
                4,                      # rating
                review.encode('utf-8'), # text
                epoch_to_iso8601(date), # pub_date
                1,                      # author_id
                increment               # shawarma_id
            ])
        else:
            continue


def create_csv(base, filename):
    with open(filename, 'wb') as w:
        writer = csv.writer(w, lineterminator='\n')
        for item in base:
                writer.writerow(item)


def create_base():

    for i in range(1, 31):
        get_review(i)

    create_csv(SHAWARMAS, 'shawarmas.csv')
    create_csv(REVIEWS, 'reviews.csv')


def remove_duplicates(filename):
    with open(filename, 'r') as in_file, open('no_duplicates.csv', 'w') as out_file:
        seen = set() # set for fast O(1) amortized lookup
        for line in in_file:
            if line in seen:
                continue
            seen.add(line)
            out_file.write(line)


if __name__ == "__main__":
    create_base()
