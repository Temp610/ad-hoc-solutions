Ad-hoc-solutions
===================================================

[54.93.174.108](http://54.93.174.108)
---------------------------------------------------

[План работы](https://docs.google.com/document/d/1UIXCwuvwjOvlhmgVLap6xVEwx-1KAiya7rlpAQx7OP0/edit)

Необходимые пакеты
---------------------------------------------------

**Перед началом необходимо установить пакеты:**

* libpq-dev
* python-dev
* nodejs
* npm

Для Ubuntu:
```shell
sudo apt-get install libpq-dev python-dev nodejs npm
```

Настройка юзера для базы
--------------------------------------------------

```shell
createuser -PE shawarma_user
```

P - опция, которая спросит password в командной строке  ( в нашем случае shawarma)

```shell
createdb -O shawarma_user -E UTF8 shawarma_db
```

-O - owner -E - encoding


Установка bower
--------------------------------------------------

При установленном nodejs и npm запускаем:

```shell
sudo npm install -g bower
```

Затем запускаем установку зависимостей:

```shell
bower install
```

С помощью этой комманды `bower` установит все зависимости из файла `bower.json` с параметрами из `.bowerrc`. Важно, чтобы установка зависимостей запускалась из корневой папки (поскольку в ней находятся `bower.json` и `.bowerrc`).

Иногда при запуске `bower` возникает ошибка `node: No such file or directory`. 
Для ее решения необходимо установить символьную ссылку с `nodejs` на `node`. Это автоматически делается при устновке:

```shell
sudo apt-get install nodejs-legacy
```

Создание фейковых данных в базе
--------------------------------------------------

* Очищаем все 3 таблицы:

```sql
DELETE FROM map_review;
DELETE FROM auth_user;
DELETE FROM map_shawarma;
```

* Сбрасываем автоинкремент:

```sql
ALTER SEQUENCE map_shawarma_id_seq RESTART WITH 1;
ALTER SEQUENCE map_review_id_seq RESTART WITH 1;
ALTER SEQUENCE auth_user_id_seq RESTART WITH 1;
```

* Запускаем скрипт генерации `generator.py` и загружаем данные из CSV в базу:

```sql
COPY map_shawarma(name, rating, address, health_inspection, latitude, longitude) FROM '/path/to/shawarmas.csv' DELIMITER ',' CSV;
COPY auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM '/path/to/users.csv' DELIMITER ',' CSV;
COPY map_review(rating, text, pub_date, author_id, shawarma_id) FROM '/path/to/reviews.csv' DELIMITER ',' CSV;
```


Установка jenkins
---------------------------------------------------

[Fast Start Guide](http://sanketdangi.com/post/62715793234/install-configure-jenkins-on-amazon-linux)

Jenkins крутится в http://54.93.174.108:8080



