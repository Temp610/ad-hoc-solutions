# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('map', '0005_auto_20151129_1934'),
    ]

    operations = [
        migrations.AlterField(
            model_name='shawarma',
            name='address',
            field=models.CharField(max_length=500),
        ),
        migrations.AlterField(
            model_name='shawarma',
            name='name',
            field=models.CharField(max_length=500),
        ),
        migrations.AlterField(
            model_name='shawarma',
            name='rating',
            field=models.DecimalField(null=True, max_digits=3, decimal_places=1, blank=True),
        ),
    ]
