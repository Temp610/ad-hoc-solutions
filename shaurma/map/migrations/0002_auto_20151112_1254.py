# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('map', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='shawarma',
            name='lat',
        ),
        migrations.RemoveField(
            model_name='shawarma',
            name='lon',
        ),
        migrations.AddField(
            model_name='shawarma',
            name='latitude',
            field=models.FloatField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='shawarma',
            name='longitude',
            field=models.FloatField(null=True, blank=True),
        ),
    ]
