# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Review',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('rating', models.DecimalField(max_digits=3, decimal_places=1)),
                ('text', models.TextField()),
                ('pub_date', models.DateTimeField(verbose_name=b'date published')),
            ],
        ),
        migrations.CreateModel(
            name='Shawarma',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('rating', models.DecimalField(max_digits=3, decimal_places=1)),
                ('address', models.CharField(max_length=100)),
                ('health_inspection', models.BooleanField()),
                ('lat', models.FloatField(null=True, verbose_name='Latitude', blank=True)),
                ('lon', models.FloatField(null=True, verbose_name='Longitude', blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='ShawarmaUser',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('login', models.CharField(max_length=50)),
                ('email', models.EmailField(max_length=254)),
                ('password', models.CharField(max_length=100)),
            ],
        ),
        migrations.AddField(
            model_name='review',
            name='author',
            field=models.ForeignKey(to='map.ShawarmaUser'),
        ),
        migrations.AddField(
            model_name='review',
            name='shawarma',
            field=models.ForeignKey(to='map.Shawarma'),
        ),
    ]
