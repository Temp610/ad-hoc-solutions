from django.db import models
from django.contrib.auth.models import User

class Shawarma(models.Model):
    name = models.CharField(max_length=500)
    rating = models.DecimalField(max_digits=3, decimal_places=1, blank=True, null=True)
    address = models.CharField(max_length=500)
    health_inspection = models.BooleanField()
    latitude = models.FloatField(blank=True, null=True)
    longitude = models.FloatField(blank=True, null=True)

    def __unicode__(self):
        return self.name


class Review(models.Model):
    id = models.AutoField(primary_key=True)
    author = models.ForeignKey(User)
    shawarma = models.ForeignKey(Shawarma)
    rating = models.DecimalField(max_digits=3, decimal_places=1)
    text = models.TextField()
    pub_date = models.DateTimeField('date published', auto_now_add=True)

    class Meta:
        unique_together = ('author', 'shawarma')

    def __unicode__(self):
        return "Review for %s by %s" % (self.shawarma, self.author)
