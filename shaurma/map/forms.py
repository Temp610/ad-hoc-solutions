from django import forms

class RegistrationForm(forms.Form):
    username = forms.CharField(label='Username', max_length=100)
    email = forms.EmailField(label='Email', max_length=100)
    password = forms.CharField(label='Password', max_length=50)

class ReviewForm(forms.Form):
    rating = forms.DecimalField(label='Rating', max_value=5, min_value=1)
    review = forms.CharField(label='Review', max_length=150)
	
