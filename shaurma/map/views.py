#coding: utf8

from django.views.decorators.csrf import ensure_csrf_cookie
from django.shortcuts import render
from django.http import HttpResponse, Http404
from django.contrib.auth import authenticate, login, logout
from django.db import IntegrityError
import json

from map.models import Shawarma, User, Review
from map.forms import RegistrationForm, ReviewForm

@ensure_csrf_cookie
def index(request):
    return render(request, 'index.html')

def get_reviews(request):
    shawarma_id = request.GET.get('id')
    if not shawarma_id or not shawarma_id.isdigit():
        return HttpResponse(
            json.dumps({
                'code': 1,
                'message': 'Wrong id'
            }), 
            content_type='application/json'
        )
    reviews = Shawarma.objects.get(id=shawarma_id).review_set.all()
    items = []
    for review in reviews:
        items.append({
            'text': review.text,
            'rating': int(review.rating),
            'user': review.author.email
        })

    return HttpResponse(json.dumps({
            'code': 0,
            'shawarma_id': shawarma_id,
            'reviews': items
        }), content_type='application/json')

def get_map_objects(request):
    shawarmas = Shawarma.objects.all()
    items = []

    bottomBorder = int(request.GET.get('bottomBorder'))
    topBorder = int(request.GET.get('topBorder'))
    showWithoutRating = json.loads(request.GET.get('showWithoutRating'))
    
    if not shawarmas:
        return HttpResponse(
            json.dumps({
                'code': 1,
                'message': 'Database is empty'
            }), 
            content_type='application/json'
        )

    for shawarma in shawarmas:
        if shawarma.rating is None and not showWithoutRating:
            continue
        else:
            if shawarma.rating is None:
                items.append({
                'cord': [shawarma.latitude, shawarma.longitude],
                'id': shawarma.id,
                'rating': None
            })

            elif bottomBorder <= shawarma.rating <= topBorder:
                items.append({
                'cord': [shawarma.latitude, shawarma.longitude],
                'id': shawarma.id,
                'rating': int(shawarma.rating)
            })

    return HttpResponse(json.dumps({
            'code': 0,
            'items': items,            
            'params': [bottomBorder, topBorder, showWithoutRating]
        }), content_type='application/json')

@ensure_csrf_cookie
def registrate_user(request):
    if request.method == "POST" and request.is_ajax():
        username = request.POST.get('username', None)
        password = request.POST.get('password', None)
        email = request.POST.get('email', None)
        form = RegistrationForm(request.POST)
        try:
            if form.is_valid():
                User.objects.create_user(username, email, password)
                user = authenticate(username=username, password=password)
                login(request, user)
                return HttpResponse(json.dumps({
                    'code': 0,
                    'message': 'Ok',
                    'username': username,
                }), content_type='application/json')
            else:
                return HttpResponse(json.dumps({
                    'code': 1,
                    'message': 'Ошибка формы'
                }), content_type='application/json')
        except IntegrityError:
            return HttpResponse(json.dumps({
                    'code': 1,
                    'message': 'Такой пользователь уже существует'
                }), content_type='application/json')
    else:
        raise Http404

@ensure_csrf_cookie
def login_user(request):
    if request.method == "POST" and request.is_ajax():
        username = request.POST.get('username', None)
        password = request.POST.get('password', None)
        user = authenticate(username=username, password=password)
        if user == request.user:
            return HttpResponse(json.dumps({
                    'code': 1,
                    'message': 'Вы уже залогинены'
                }), content_type='application/json')
        if user is not None:
            login(request, user)
            return HttpResponse(json.dumps({
                    'code': 0,
                    'username': username,
                    'message': 'Ok'
                }), content_type='application/json')
        else:
            return HttpResponse(json.dumps({
                    'code': 1,
                    'message': 'Ошибка авторизации'
                }), content_type='application/json')
    else: 
        raise Http404

def logout_user(request):
    if request.is_ajax():
        if request.user.username:
            logout(request)
            return HttpResponse(json.dumps({
                    'code': 0,
                    'message': 'Ok'
                }), content_type='application/json')
        else:
            return HttpResponse(json.dumps({
                    'code': 1,
                    'message': 'No user to logout'
                }), content_type='application/json')
    else:
        raise Http404    

def add_review(request, shawarma_id):
    if request.method == "POST" and request.is_ajax():
        if request.user.username:
            
            review = request.POST.get('review', None)
            rating = request.POST.get('rating', None)
            form = ReviewForm(request.POST)

            try:
                shawarma = Shawarma.objects.get(pk=shawarma_id)
            except:
                raise Http404

            try:
                if form.is_valid():
                    Review.objects.create(
                        author=request.user,
                        shawarma=shawarma,
                        rating=rating,
                        text=review
                    )
                    return HttpResponse(json.dumps({
                        'code': 0,
                        'message': 'Спасибо за ваш отзыв!',
                        'review': {
                            'user': request.user.email,
                            'rating': rating,
                            'text': review
                        }
                    }), content_type='application/json')
                else:
                    return HttpResponse(json.dumps({
                        'code': 1,
                        'message': 'Ошибка формы',
                        'data': request.POST,
                    }), content_type='application/json')
            except IntegrityError as error:
                return HttpResponse(json.dumps({
                        'code': 1,
                        'message': 'Вы уже добавлили отзыв'
                    }), content_type='application/json')

        else:
            return HttpResponse(json.dumps({
                    'code': 1,
                    'message': 'Пожалуйста залогиньтесь'
                }), content_type='application/json')
    else:
        raise Http404

@ensure_csrf_cookie
def add_pin(request):
    if request.method == "POST" and request.is_ajax():
        if request.user.username:

            lat = request.POST.get('lan', None)
            lon = request.POST.get('lon', None)
            name = request.POST.get('name', None)
            address = request.POST.get('address', None)

            shawarma = Shawarma.objects.create(
                latitude=lat,
                longitude=lon,
                name = name,
                address = address,
                health_inspection = 1
            )
            return HttpResponse(json.dumps({
                            'code': 0,
                            'message': 'Место добавлено!',
                            'shawarma_id': shawarma.id
                        }), content_type='application/json')

        else:
            return HttpResponse(json.dumps({
                    'code': 1,
                    'message': 'Пожалуйста залогиньтесь'
                }), content_type='application/json')
    else:
        raise Http404

