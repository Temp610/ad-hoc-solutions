from django.contrib import admin

from .models import Shawarma, Review


class ReviewInline(admin.StackedInline):
    model = Review
    extra = 3


class ShawarmaAdmin(admin.ModelAdmin):
    list_display = ('name', 'rating', 'address',
                    'health_inspection', 'latitude', 'longitude')
    inlines = [ReviewInline]
    search_fields = ['name', 'address']


class ReviewAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,               {'fields': ['author', 'shawarma',
                                         'rating', 'text']}),
        ('Date information', {'fields': ['pub_date'],
                              'classes': ['collapse']}),
    ]
    list_display = ('author', 'shawarma', 'rating', 'pub_date')
    list_filter = ['pub_date']
    search_fields = ['author', 'shawarma', 'text', 'pub_date']

admin.site.register(Shawarma, ShawarmaAdmin)
admin.site.register(Review, ReviewAdmin)
