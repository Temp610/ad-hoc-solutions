"""shaurma URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.auth.views import logout
from map import views

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', views.index, name='index'),
    url(r'^api/map/get_objects/$', views.get_map_objects, name='get_map_objects'),
    url(r'^api/map/get_reviews/', views.get_reviews, name='get_reviews'),
    url(r'^api/map/login/', views.login_user, name='login_user'),
    url(r'^api/map/signin/', views.registrate_user, name='registrate_user'),
    url(r'^api/map/logout/', views.logout_user, name='logout_user'),
    url(r'^api/map/add_review/(((?P<shawarma_id>[^/]+)/)?)$', views.add_review, name='add_review'),
    url(r'^api/map/add_pin/', views.add_pin, name='add_pin')
]
