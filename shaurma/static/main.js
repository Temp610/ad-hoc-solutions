ymaps.ready(init);
var myMap;
var isAuthenticated = 0;
var balloonMaxHeight = 500;
var myPlacemarks = []
var loginButton;
var addedPin;

$.views.settings.delimiters("{?", "?}");

$(document).on('closed', '.remodal', function () {
    $('.b-modal__alert').animate({
        'opacity': 0.00001
    })
});


function getPostAjaxRequest(form, url, completeCallback, params) {
    $.ajax({
        url: url,
        type: 'POST',
        data: form.serialize(),
        beforeSend: function(xhr, settings) {
            xhr.setRequestHeader("X-CSRFToken", Cookies.get('csrftoken'));
            if (params) {
                if (form) {
                    options = '&' + $.param(params)
                    settings.data += options;
                }
                else {
                    settings.data += $.param(params)
                }
            }

        },
        success: function(data) {
            if (data.code == 0) {
                completeCallback(data);
            }
            else {
                form.children('.b-modal__alert').text(data.message)
                form.children('.b-modal__alert').animate({
                    'duration': 'slow',
                    'opacity': 1
                })
                console.log(data.message);
            }
        }
    });
}

/* При загрузке документа */


$(document).ready(function() {

    if ($('#map').data('is-authenticated') == 'True') {
        isAuthenticated = 1;
    }

    $(".b-modal__tabs").steps({
        headerTag: "h3",
        bodyTag: "section",
        transitionEffect: "slideLeft",
        enableFinishButton: false,
        enablePagination: false,
        enableAllSteps: true,
        titleTemplate: "#title#",
        cssClass: "tabcontrol"
    });

    $('.b-user__logout').click(function() {
        $.ajax({
            url: '/api/map/logout',
            dataType: "json",
            success: function() {
                $('.b-user').fadeOut('slow', function() { renderLoginButton(); });
                isAuthenticated = 0;
            }
        });
    })

    $('.b-sort__btn').click(function() {
        $(".b-sort").toggle('drop', { direction: 'right' }, 'slow')
    })

    $('.b-sort__submit').click(function() {
        var checkboxValue = $('.b-sort input[type=checkbox]').is(':checked')
        var sliderValues = $('.b-sort .slider-range').slider('values');

        Cookies.set('showWithoutRating', checkboxValue)
        Cookies.set('shawarmaRating', sliderValues)

        objectManager.removeAll()
        renderPlacemarks();
    })

    $('.b-sort .slider-range').slider({
        range: true,
        min: 0,
        max: 5,
        values: [ 0, 5 ],
        slide: function( event, ui ) {
            $("#amount").text( ui.values[ 0 ] + " - " + ui.values[ 1 ] );
        }
    });

    var loginForm = $('.b-modal__form-login');
    loginForm.submit(function(){
        getPostAjaxRequest(loginForm, '/api/map/login/', function(data) {
            loginForm.children('.b-modal__alert').animate({
                'duration': 'slow',
                'opacity': 0.00001
            }, function() {
                isAuthenticated = 1;
                myMap.controls.remove(loginButton)
                $('.b-user__username').text(data.username);
                $('.b-user').fadeIn();
                $('.b-modal-authenticate').remodal().close();
            });
        });
        return false;
    });

    var signinForm = $('.b-modal__form-signin');
    signinForm.submit(function(){
        getPostAjaxRequest(signinForm, '/api/map/signin/', function(data) {
            signinForm.children('.b-modal__alert').animate({
                'duration': 'slow',
                'opacity': 0.00001
            }, function() {                
                isAuthenticated = 1;
                myMap.controls.remove(loginButton)
                $('.b-user__username').text(data.username)
                $('.b-user').fadeIn();
                $('.b-modal-authenticate').remodal().close()
            });
        });
        return false;
    });

    var reviewForm = $('.b-modal__form-review');
    reviewForm.submit(function(){
        shawarmaId = $('.b-placemark').data('shawarmaId')
        getPostAjaxRequest(reviewForm, '/api/map/add_review/' + shawarmaId + '/', function(data) {
            var tmpl = $.templates("#reviewTemplate");
            var html = tmpl.render(data.review);
            $('.b-modal-addReview').remodal().close()
            $(html).insertBefore('.b-placemark__review-add');

            // Избавиться, как только поменяем дизайн формы
            var currentHeight = parseInt($('.b-placemark').css('height'));
            if (currentHeight < balloonMaxHeight) {
                var previousHeight = parseInt($('ymaps[class*="balloon__content"]').children('ymaps').css('height'))
                $('ymaps[class*="balloon__content"]').children('ymaps').css('height', currentHeight)
                var currentTopPosition = parseInt($('ymaps[class*="balloon "]').css('top'));
                $('ymaps[class*="balloon "]').css('top', currentTopPosition - (currentHeight - previousHeight) + 'px')
            }

            renderBalloon(shawarmaId)
        });
        return false;
    });

    var addPinForm = $('.b-modal__form-addPin');
    addPinForm.submit(function() {
        cord = addedPin.geometry.getCoordinates()
        getPostAjaxRequest(addPinForm, '/api/map/add_pin/', function(data) {

            myMap.geoObjects.remove(addedPin);
            addedPin = null;

            $('.b-modal-addPin').remodal().close()

            renderPlacemark(cord, data.shawarma_id, null);

        }, {lan: cord[0], lon: cord[1]} );
        return false;
    });
})



/* При загрузке карты */


function init(){

    myCollection = new ymaps.GeoObjectCollection()

    objectManager = new ymaps.ObjectManager({
        clusterize: true,
        preset: 'islands#invertedBlueClusterIcons',
        groupByCoordinates: false,
        clusterOpenBalloonOnClick: true,
        gridSize: 128,
        maxZoom: 14,
        geoObjectOpenBalloonOnClick: false
    });

    objectManager.objects.options.set('balloonMaxHeight', balloonMaxHeight);
    objectManager.objects.options.set('balloonCloseButton', false);
    objectManager.objects.options.set('balloonMinWidth', 300);

    myMap = new ymaps.Map("map", {
        center: [55.76, 37.64],
        zoom: 10,
        controls: ["geolocationControl","rulerControl","searchControl","zoomControl"]
    });

    myMap.options.set('dragCursor', 'grab');

    myMap.events.add('click', function (e) {

        $(".b-placemark").animate({
            'duration': 'slow',
            'opacity': 0.00001
        }, function() {
            myMap.balloon.close();
        });

        if ($('.b-map').hasClass('addMode')) {
            if (addedPin) {
                addedPin.geometry.setCoordinates(e.get('coords'))
            }
            else {
                addedPin = new ymaps.GeoObject({
                    geometry: {
                        type: "Point",
                        coordinates: e.get('coords')
                    },
                    properties: {
                        hintContent: 'Нажмите, чтобы добавить отзыв'
                    }
                    }, {
                        iconLayout: 'default#image',
                        iconImageHref: 'static/images/pin_new.png',
                        iconImageSize: [50, 90],
                        iconImageOffset: [-25, -87.5],
                        draggable: true
                });
                addedPin.events.add('click', function(e) {
                    if (isAuthenticated) {
                        $('.b-modal-addPin').remodal().open()
                    }
                    else {
                        e.preventDefault()
                        $(document).on('closed', '.b-modal-authenticate', function () {
                            if ( isAuthenticated == 1) {
                                addedPin.events.fire('click')
                            }
                            $(document).off('closed', '.b-modal-authenticate')
                        });
                        $('.b-map__btn-login').click()
                    }
                })
                myMap.geoObjects.add(addedPin)
            }
        }
    });


    renderModeButton();
    renderPlacemarks();
    if ( isAuthenticated == 1) {
        $('.b-user').show()
    }
    else {
        renderLoginButton();
    }
    $('.b-sort__btn').show()

}

function renderLoginButton() {
    var tmpl = $.templates("#buttonTemplate");
    var ButtonLayout = ymaps.templateLayoutFactory.createClass(tmpl.render());
    loginButton = new ymaps.control.Button({
        options: {
            layout: ButtonLayout,
            position: { right: 15, top: 60 }
        }   
    });
    myMap.controls.add(loginButton);
}

function renderModeButton() {
    var ButtonLayout = ymaps.templateLayoutFactory.createClass('<div class="b-map__btn-add btn btn-default">Режим просмотра</div>');
    var modeButton = new ymaps.control.Button({
        options: {
            layout: ButtonLayout
        }   
    });
    modeButton.events.add('click', function() {
        $('.b-map').toggleClass('viewMode addMode');

        if (addedPin) {
            myMap.geoObjects.remove(addedPin);
            addedPin= null;
        }

        if ($('.b-map').hasClass('viewMode')) {
            myMap.options.set('dragCursor', 'grab');
            $('.b-map__btn-add').text('Режим просмотра');
        }
        else {
            myMap.options.set('dragCursor', 'crosshair');
            $('.b-map__btn-add').text('Режим добавления');
        }
    });

    myMap.controls.add(modeButton, {float: 'right'});
}

function renderBalloon(id) {
    var tmpl = $.templates("#pinTemplate");
    var object = objectManager.objects.getById(id);
    $.ajax({
        url: '/api/map/get_reviews/?id=' + id,
        dataType: "json",
        success: function(data) {
            if (data.code == 0) {                
                object.properties.balloonContent = tmpl.render(data);
                objectManager.objects.balloon.open(id);
            }
            else {
                console.log(data.message);
            }
        }
    });
}


function showBalloonContent() {

    $('.b-placemark__text').readmore({
        speed: 500,
        collapsedHeight: 60,
        moreLink: '<a href="#">Показать полностью</a>',
        lessLink: '<a href="#">Скрыть</a>',

        beforeToggle: function(trigger, element, expanded) {

        },

        // Если после раскрытия текста высота балуна меньше максимальной - увеличиваем его высоту 
        // и смещаем его точку привязки
        afterToggle: function(trigger, element, expanded) {

            var currentHeight = parseInt(element.closest('.b-placemark').css('height'));

            if (currentHeight < balloonMaxHeight) {             
                var previousHeight = parseInt(element.closest('ymaps[class*="balloon__content"]').children('ymaps').css('height'))
                element.closest('ymaps[class*="balloon__content"]').children('ymaps').css('height', currentHeight)
                var currentTopPosition = parseInt(element.closest('ymaps[class*="balloon "]').css('top'));
                element.closest('ymaps[class*="balloon "]').css('top', currentTopPosition - (currentHeight - previousHeight) + 'px')
            }
        }
    });

    $(".b-placemark__review-add").click(function(e) {
        if ( isAuthenticated == 0) {
            e.preventDefault()
            $(document).on('closed', '.b-modal-authenticate', function () {
                if ( isAuthenticated == 1) {
                    $(".b-placemark__review-add-btn").click();
                }
                $(document).off('closed', '.b-modal-authenticate')
            });
            $('.b-map__btn-login').click()
        }
    })

    $(".b-placemark").animate({
        'duration': 'slow',
        'opacity': 1
    });
}

function renderPlacemark(cord, id, rating) {
    objectManager.add({
        type: 'Feature',
        id: id,
        geometry: {
            type: 'Point',
            coordinates: cord
        },
        options: {
            iconLayout: 'default#image',
            iconImageHref: 'static/images/pin_' + rating + '.png',
            iconImageSize: [50, 90],
            iconImageOffset: [-25, -87.5]
        }
    });
}


function renderPlacemarks() {

    var rating = Cookies.get('shawarmaRating')
    if (rating === undefined) {
        rating = [0, 5]
        Cookies.set('showWithoutRating', [0, 5])
    } else {
        rating = JSON.parse(rating)
    }

    var showWithoutRating = Cookies.get('showWithoutRating')
    if (showWithoutRating === undefined) {
        showWithoutRating = true
        Cookies.set('showWithoutRating', true)
    }
    $.ajax({
        url: '/api/map/get_objects/',
        dataType: "json",
        data: ({
            bottomBorder: rating[0],
            topBorder: rating[1],
            showWithoutRating: showWithoutRating
        }),
        success: function(data) {
            if (data.code == 0) {
                data.items.forEach(function(element) {
                    renderPlacemark(element.cord, element.id, element.rating)
                });

                objectManager.objects.events.add('click', function(e) {
                    var objectId = e.get('objectId');
                    object = objectManager.objects.getById(objectId);
                    if (object.properties.balloonContent != undefined) {
                        objectManager.objects.balloon.open(objectId);
                    } else {
                        renderBalloon(objectId, object);
                    }
                });

                objectManager.objects.events.add('balloonopen', function(e) {
                    showBalloonContent();
                });

                myMap.geoObjects.add(objectManager);
            }
            else {
                console.log(data.message);
            }
        }
    });
}